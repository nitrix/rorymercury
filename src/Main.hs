{-# LANGUAGE OverloadedStrings #-}

import Data.IORef
import qualified Data.Configurator as C
import qualified Data.ByteString.Char8 as B
import Network.SimpleIRC
import System.Random

import Context
import Events


-- TODO: save current channels somehow? conflict with config?
-- TODO: handle error code 433 nick already in use
-- TODO: modify the whole activity map thing
-- TODO: anti message spam, anti join/part spam, anti nick change spam, anti hilight spam
-- TODO: problematic language scouting, idiot/stupid/moron/fuck/dick/etc...
-- TODO: regulars that we can voice in case of lockdown
-- TODO: detect netsplits (quit reason "*.net *.split") and grant +o to ops? Also command to do it?
-- TODO: lockdown mode (grant +v to regulars, trustees and ops)

main = do
    -- Load configuration
    putStrLn "Loading configuration..."
    config <- C.load [C.Optional "rorymercury.conf"]
    realname <- C.lookupDefault "Rory Mercury" config "realname"
    number <- randomRIO (10000, 99999) :: IO Int
    username <- C.lookupDefault ("Guest" ++ (show number)) config "username"
    password <- C.lookup config "password"
    hostname <- C.lookupDefault "irc.freenode.org" config "hostname"
    port <- C.lookupDefault 7000 config "port"
    secure <- C.lookupDefault True config "secure"
    channels <- C.lookupDefault ["#rory"] config "channels"

    -- Resuming context
    putStrLn "Resuming context..."
    context <- resumeContext

    -- Connect to server
    putStrLn "Connecting to server..."
    connect (mkDefaultConfig hostname username)
        { cChannels = channels
        , cPass = password
        , cEvents = events context
        , cRealname = realname
        , cUsername = username
        , cSecure = secure
        , cPort = port
        } False False
