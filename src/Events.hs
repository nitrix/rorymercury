module Events where

import Network.SimpleIRC
import Control.Monad
import Data.IORef
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.Encoding.Error as T
import qualified Data.ByteString.Char8 as B

import Context
import Instructions

events :: Contextual [IrcEvent]
events ctx = [ Numeric $ onNumeric ctx
             , Join $ onJoin ctx
             , Part $ onPart ctx
             , Privmsg $ onMessage ctx
             , Quit $ onQuit ctx
             , Kick $ onKick ctx
             ]

onNumeric :: Contextual EventFunc
onNumeric ctx srv msg 
    | code == 353 = onNamesReply ctx srv msg
    | code == 366 = onNamesEndReply ctx srv msg
    | otherwise = return ()
    where
        code = read $ B.unpack $ mCode msg

onMessage :: Contextual EventFunc
onMessage ctx srv msg = sequence_ $ do
    nick <- B.unpack <$> mNick msg -- Who is talking
    chan <- B.unpack <$> mChan msg -- Where are we talking
    origin <- B.unpack <$> mOrigin msg -- Who to reply to
    return $ if isAnInstruction content
             then processInstruction ctx srv msg
             else ctxIncChannelActivity ctx chan nick
    where
        isAnInstruction x = maybe False (==',') $ listToMaybe x
        content = T.unpack $ T.decodeUtf8With T.ignore $ mMsg msg

onJoin :: Contextual EventFunc
onJoin ctx srv msg = sequence_ $ do
    nick <- B.unpack <$> mNick msg
    return $ ctxAddChannelNames ctx chan [nick]
    where
        chan = B.unpack $ mMsg msg

onPart :: Contextual EventFunc
onPart ctx srv msg = sequence_ $ do
    nick <- B.unpack <$> mNick msg
    chan <- B.unpack <$> mChan msg
    return $ ctxRemChannelNames ctx chan [nick]

onQuit :: Contextual EventFunc
onQuit ctx srv msg = sequence_ $ do
    nick <- B.unpack <$> mNick msg
    return $ ctxRemNameFromAllChannels ctx nick

onKick :: Contextual EventFunc
onKick ctx srv msg = sequence_ $ do
    chan <- B.unpack <$> mChan msg
    nick <- B.unpack <$> (mOther msg >>= listToMaybe) -- TODO: slightly disgusting
    return $ ctxRemChannelNames ctx chan [nick]

onNamesReply :: Contextual EventFunc
onNamesReply ctx srv msg = sequence_ $ do
    chan <- listToMaybe parsed
    return $ ctxAddChannelNames ctx chan names
    where
        sanitize full@(x:xs) = if x `elem` ":.!@~%+" then sanitize xs else full
        names = map sanitize $ drop 1 parsed
        parsed = words . B.unpack $ mMsg msg

onNamesEndReply :: Contextual EventFunc
onNamesEndReply ctx srv msg = sequence_ $ do
    chan <- B.unpack <$> (mOther msg >>= listToMaybe) -- TODO: slightly disgusting
    return $ putStrLn $ "Joined channel " ++ chan
