module Instructions where

import Network.SimpleIRC
import qualified Data.ByteString.Char8 as B
import Data.Maybe

import Context

processInstruction :: Contextual EventFunc
processInstruction ctx srv msg = do
    case instruction of
        Just "quit" -> requirePerm admin $ quitInstruction ctx srv msg
        Just "ctx" -> requirePerm admin $ ctxDebug ctx
        Just "join" -> requirePerm admin $ joinInstruction ctx srv msg
        Just "part" -> requirePerm admin $ partInstruction ctx srv msg
        Just _ -> return () -- TODO: unknown instruction, warn ops?
        Nothing -> return ()
    where
        trustee = op -- TODO: set of trustees
        op = admin -- TODO: set of operators
        admin = maybe False (== "nitrix") maybeNick -- TODO: set of admins
        instruction = listToMaybe parsed
        arguments = drop 1 parsed
        parsed = words $ drop 1 $ B.unpack $ mMsg msg
        requirePerm p f = if p then f else return () -- TODO: Alert ops
        maybeNick = B.unpack <$> mNick msg -- Who is talking
        maybeChan = B.unpack <$> mChan msg -- Where are we talking
        maybeOrigin = B.unpack <$> mOrigin msg -- Who to reply to

quitInstruction ctx srv msg = do
    putStrLn $ "Shutting down (" ++ reason ++ ")..."
    disconnect srv $ B.pack reason
    where
        reason = if null givenReason then "Forcefully shutting down" else givenReason
        givenReason = unwords arguments
        arguments = drop 1 parsed
        parsed = words $ drop 1 $ B.unpack $ mMsg msg

joinInstruction ctx srv msg = do
    -- TODO: key support
    maybe (return()) (\x -> sendCmd srv $ MJoin (B.pack x) Nothing) chan
    where
        chan = listToMaybe arguments
        arguments = drop 1 parsed
        parsed = words $ drop 1 $ B.unpack $ mMsg msg

partInstruction ctx srv msg = do
    -- TODO: part msg
    maybe (return()) (\x -> sendCmd srv $ MPart (B.pack x) (B.pack "Parted")) chan
    where
        chan = listToMaybe arguments
        arguments = drop 1 parsed
        parsed = words $ drop 1 $ B.unpack $ mMsg msg
