module Context where

import Data.IORef
import Data.Map as M
import Data.Set as S
import Network.SimpleIRC

data Context = MkContext { channelNames :: Map String (Set String) 
                         , channelActivity :: Map String (Map String Int) -- TODO: decreases every hours

                         , channelRegulars :: Map String (Set String) -- permanent, requires 50 activity points
                         , channelTrustees :: Map String (Set String) -- manual, can vote
                         , channelOps :: Map String (Set String) -- manual, direct operations
                         , admins :: Set String -- everything including system stuff
                         } deriving (Show)

type Contextual a = IORef Context -> a

resumeContext :: IO (IORef Context)
resumeContext = newIORef $ MkContext M.empty M.empty M.empty M.empty M.empty S.empty -- TODO: import/export those

ctxAddChannelNames :: IORef Context -> String -> [String] -> IO ()
ctxAddChannelNames ctx chan names = do
    atomicModifyIORef ctx (\x -> (x { channelNames = op $ channelNames x },()))
    where
        op = M.insertWith S.union chan (S.fromList names)

ctxRemChannelNames :: IORef Context -> String -> [String] -> IO ()
ctxRemChannelNames ctx chan names = do
    atomicModifyIORef ctx (\x -> (x { channelNames = op $ channelNames x },()))
    where
        -- TODO: to fix to prevent empty sets
        op = M.insertWith (flip S.difference) chan (S.fromList names)

ctxRemNameFromAllChannels :: IORef Context -> String -> IO ()
ctxRemNameFromAllChannels ctx name = do
    atomicModifyIORef ctx (\x -> (x { channelNames = op $ channelNames x },()))
    where
        op = M.mapMaybe (\x -> let m = S.delete name x in if S.null m then Nothing else Just m)

ctxIncChannelActivity :: IORef Context -> String -> String -> IO ()
ctxIncChannelActivity ctx chan name = do
    atomicModifyIORef ctx (\x -> (x { channelActivity = op $ channelActivity x },()))
    where
        op = M.insertWith (M.unionWith (+)) chan (M.fromList [(name, 1)])

ctxDecChannelActivity :: IORef Context -> String -> IO ()
ctxDecChannelActivity ctx chan = do
    atomicModifyIORef ctx (\x -> (x { channelActivity = op $ channelActivity x },()))
    where
        op = M.mapMaybe (\x -> let m = op2 x in if M.null m then Nothing else Just m)
        op2 = M.mapMaybe (\x -> let n = x-1 in if n > 0 then Just n else Nothing)

ctxDebug :: IORef Context -> IO ()
ctxDebug ctx = do
    x <- readIORef ctx
    print x
